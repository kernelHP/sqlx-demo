package main

import (
	"database/sql/driver"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"strings"
)

type user struct {
	ID   uint64
	Age  uint16
	Name string
}

// 使用sqlx.In实现批量插入
// 前提是需要我们的结构体实现driver.Valuer接口：
func (u user) Value() (driver.Value, error) {
	return []interface{}{u.Name, u.Age}, nil
}

var db *sqlx.DB

func InitDB() (err error) {

	fmt.Println("************************** init db connection ********************************")

	dns := "root:root@tcp(127.0.0.1:3306)/test?charset=utf8mb4&parseTime=true&loc=Local"
	//db, err = sqlx.Connect("mysql", dns)
	//if err != nil {
	//	panic(fmt.Sprintf("connect database failed err:%v\n", err.Error()))
	//}

	db = sqlx.MustConnect("mysql", dns)
	db.SetMaxOpenConns(30)
	db.SetMaxIdleConns(10)

	if err = db.Ping(); err != nil {
		panic(fmt.Sprintf("connect database ping failed err:%v\n", err.Error()))
		return
	}

	return
}

func QueryRowDemo() {

	fmt.Println("************************** query single row ********************************")

	var u user
	if err := db.Get(&u, "select * from `user` where id = ?", 3); err != nil {
		fmt.Printf("query row err:%v\n", err)
		return
	}

	fmt.Printf("id:%d , age:%d , name:%s\n", u.ID, u.Age, u.Name)
}

// 查询多条数据示例
func QueryMultiRowDemo() {
	fmt.Println("************************** query multi row ********************************")

	sqlStr := "select id, name, age from user where id > ?"
	var users []user
	err := db.Select(&users, sqlStr, 0)
	if err != nil {
		fmt.Printf("query failed, err:%v\n", err)
		return
	}
	fmt.Printf("users:%v\n", users)
}

// 插入数据
func InsertRowDemo() {

	fmt.Println("************************** insert row ********************************")

	sqlStr := "insert into user(name, age) values (?,?)"
	ret, err := db.Exec(sqlStr, "沙河小王子", 19)
	if err != nil {
		fmt.Printf("insert failed, err:%v\n", err)
		return
	}
	theID, err := ret.LastInsertId() // 新插入数据的id
	if err != nil {
		fmt.Printf("get lastinsert ID failed, err:%v\n", err)
		return
	}
	fmt.Printf("insert success, the id is %d.\n", theID)
}

// 更新数据
func UpdateRowDemo() {
	fmt.Println("************************** update row ********************************")

	sqlStr := "update user set age=? where id = ?"
	ret, err := db.Exec(sqlStr, 39, 6)
	if err != nil {
		fmt.Printf("update failed, err:%v\n", err)
		return
	}
	n, err := ret.RowsAffected() // 操作影响的行数
	if err != nil {
		fmt.Printf("get RowsAffected failed, err:%v\n", err)
		return
	}
	fmt.Printf("update success, affected rows:%d\n", n)
}

// 删除数据
func DeleteRowDemo() {

	fmt.Println("************************** delete row ********************************")

	sqlStr := "delete from user where id = ?"
	ret, err := db.Exec(sqlStr, 6)
	if err != nil {
		fmt.Printf("delete failed, err:%v\n", err)
		return
	}
	n, err := ret.RowsAffected() // 操作影响的行数
	if err != nil {
		fmt.Printf("get RowsAffected failed, err:%v\n", err)
		return
	}
	fmt.Printf("delete success, affected rows:%d\n", n)
}

func InsertUserDemo() (err error) {

	fmt.Println("************************** named insert row ********************************")

	sqlStr := "INSERT INTO user (name,age) VALUES (:name,:age)"
	_, err = db.NamedExec(sqlStr,
		map[string]interface{}{
			"name": "七米",
			"age":  28,
		})
	return
}

func NamedQuery() {

	fmt.Println("************************** named query row ********************************")

	sqlStr := "SELECT * FROM user WHERE name=:name"
	// 使用map做命名查询
	rows, err := db.NamedQuery(sqlStr, map[string]interface{}{"name": "七米"})
	if err != nil {
		fmt.Printf("db.NamedQuery failed, err:%v\n", err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var u user
		err := rows.StructScan(&u)
		if err != nil {
			fmt.Printf("scan failed, err:%v\n", err)
			continue
		}
		fmt.Printf("user:%v\n", u)
	}

	u := user{
		Name: "七米",
	}
	// 使用结构体命名查询，根据结构体字段的 db tag进行映射
	rows, err = db.NamedQuery(sqlStr, u)
	if err != nil {
		fmt.Printf("db.NamedQuery failed, err:%v\n", err)
		return
	}
	defer rows.Close()
	for rows.Next() {
		var u user
		err := rows.StructScan(&u)
		if err != nil {
			fmt.Printf("scan failed, err:%v\n", err)
			continue
		}
		fmt.Printf("user:%v\n", u)
	}
}

func TransactionDemo() (err error) {

	fmt.Println("************************** Transaction demo ********************************")

	tx, err := db.Beginx() // 开启事务
	if err != nil {
		fmt.Printf("begin trans failed, err:%v\n", err)
		return err
	}
	defer func() {
		if p := recover(); p != nil {
			tx.Rollback()
			panic(p) // re-throw panic after Rollback
		} else if err != nil {
			fmt.Printf("rollback , err:%vn", err)
			tx.Rollback() // err is non-nil; don't change it
		} else {
			err = tx.Commit() // err is nil; if Commit returns error update err
			fmt.Println("commit")
		}
	}()

	sqlStr1 := "Update user set age=20 where id=?"

	rs, err := tx.Exec(sqlStr1, 3)
	if err != nil {
		return err
	}
	n, err := rs.RowsAffected()
	if err != nil {
		return err
	}
	if n != 1 {
		return errors.New("exec sqlStr1 failed")
	}
	sqlStr2 := "Update user set age=50 where id = ?"
	rs, err = tx.Exec(sqlStr2, 5)
	if err != nil {
		return err
	}
	n, err = rs.RowsAffected()
	if err != nil {
		return err
	}
	if n != 1 {
		return errors.New("exec sqlStr1 failed")
	}
	return err
}

// BatchInsertUsers 自行构造批量插入的语句
func BatchInsertUsers1(users []*user) error {

	fmt.Println("************************** BatchInsertUsers1 demo ********************************")

	// 存放 (?, ?) 的slice
	valueStrings := make([]string, 0, len(users))
	// 存放values的slice
	valueArgs := make([]interface{}, 0, len(users)*2)
	// 遍历users准备相关数据
	for _, u := range users {
		// 此处占位符要与插入值的个数对应
		valueStrings = append(valueStrings, "(?, ?)")
		valueArgs = append(valueArgs, u.Name)
		valueArgs = append(valueArgs, u.Age)
	}
	// 自行拼接要执行的具体语句
	stmt := fmt.Sprintf("INSERT INTO user (name, age) VALUES %s",
		strings.Join(valueStrings, ","))
	_, err := db.Exec(stmt, valueArgs...)
	return err
}

// BatchInsertUsers2 使用sqlx.In帮我们拼接语句和参数, 注意传入的参数是[]interface{}
func BatchInsertUsers2(users []interface{}) error {

	fmt.Println("************************** BatchInsertUsers2 demo ********************************")

	query, args, _ := sqlx.In(
		"INSERT INTO user (name, age) VALUES (?), (?), (?)",
		users..., // 如果arg实现了 driver.Valuer, sqlx.In 会通过调用 Value()来展开它
	)
	fmt.Println(query) // 查看生成的querystring
	fmt.Println(args)  // 查看生成的args
	_, err := db.Exec(query, args...)
	return err
}

// QueryByIDs 根据给定ID查询
func QueryByIDs(ids []int) (users []user, err error) {

	fmt.Println("************************** query sqlx in ********************************")

	// 动态填充id
	query, args, err := sqlx.In("SELECT id , name, age FROM user WHERE id IN (?)", ids)
	if err != nil {
		return
	}
	// sqlx.In 返回带 `?` bindvar的查询语句, 我们使用Rebind()重新绑定它
	query = db.Rebind(query)

	err = db.Select(&users, query, args...)
	fmt.Printf("users:%v\n", users)
	return
}

// QueryAndOrderByIDs 按照指定id查询并维护顺序
func QueryAndOrderByIDs(ids []int) (users []user, err error) {

	fmt.Println("************************** query sqlx in FIND_IN_SET ********************************")

	// 动态填充id
	strIDs := make([]string, 0, len(ids))
	for _, id := range ids {
		strIDs = append(strIDs, fmt.Sprintf("%d", id))
	}
	query, args, err := sqlx.In("SELECT id , name, age FROM user WHERE id IN (?) ORDER BY FIND_IN_SET(id, ?)", ids, strings.Join(strIDs, ","))
	if err != nil {
		return
	}

	// sqlx.In 返回带 `?` bindvar的查询语句, 我们使用Rebind()重新绑定它
	query = db.Rebind(query)

	err = db.Select(&users, query, args...)
	fmt.Printf("users:%v\n", users)

	return
}

func main() {

	if err := InitDB(); err != nil {
		panic(err)
	}

	QueryRowDemo()
	QueryMultiRowDemo()
	InsertRowDemo()
	UpdateRowDemo()
	DeleteRowDemo()
	InsertUserDemo()
	NamedQuery()
	TransactionDemo()

	u1 := user{Name: "七米", Age: 18}
	u2 := user{Name: "q1mi", Age: 28}
	u3 := user{Name: "小王子", Age: 38}

	users := []*user{&u1, &u2, &u3}
	err := BatchInsertUsers1(users)
	if err != nil {
		fmt.Printf("BatchInsertUsers failed, err:%v\n", err)
	}

	// 方法2
	users2 := []interface{}{u1, u2, u3}
	err = BatchInsertUsers2(users2)
	if err != nil {
		fmt.Printf("BatchInsertUsers2 failed, err:%v\n", err)
	}

	ids := []int{1, 3, 5}
	QueryByIDs(ids)

	QueryAndOrderByIDs(ids)
}
